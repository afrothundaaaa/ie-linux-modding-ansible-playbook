# Infinity Engine Linux Modding
This project is simply an ansible playbook that will setup a usable modding environment for Infinity Engine Enhanced Edition Games (bgee, bg2ee, iwdee) by creating a .img file at ~/Games/infinity-engine-games.img and setting this file to auto mount on boot.  It will additionally find any downloaded EE games and migrate them to the new location, and symlink them so steam can still launch games as normal.

This will additionally download the latest weidu binaries for linux and extract them if there is no weidu file found at /Games/infinity-engine/tools (mount point of the .img file after mounting)

## Usage
Simply clone this repo and run it with ansible.

EX:

```
git clone git@gitlab.com:afrothundaaaa/ie-linux-modding-ansible-playbook.git
cd ie-linux-modding-ansible-playbook
ansible-playbook create-ee-vdisk.yml
````